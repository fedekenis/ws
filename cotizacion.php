<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Divisas</title>                       
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="resources/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
<?php
ini_set('display_errors', 0);
//Setea zona horaria para informar correctamente hora de error.
ini_set('date.timezone', 'America/Argentina/Buenos_Aires');

//COTIZACIONES.--------------------------------------------------
$url= 'https://www.dolarsi.com/api/api.php?type=valoresprincipales';
$data= file_get_contents($url);

if ($data){      
    //print_r($data); die();
    $data = json_decode($data);
    
    if($data){
        ?>
        <!--Comienza tabla -->
        <h3 class="text-center">Cotizaciones</h3>
        <div class="container text-center">        
            <table class="table table-responsive table-bordered table-sm table-xs">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Compra</th>
                        <th class="text-center">Venta</th>                       
                    </tr>
                </thead>
            <?php
            for($i=0; $i<7; $i++){
                ?>
            <tbody>
                <tr>
                    <td><?php echo $i+1 ?></td>  
                    <td><?php echo $data[$i]->casa->nombre;  ?></td>
                    <td><?php echo $data[$i]->casa->compra;  ?></td>
                    <td><?php echo $data[$i]->casa->venta;  ?></td>                                                
                </tr>
            </tbody>
            <?php
            }            
            ?>                                 
            </table>        
        </div>
        <!--Finaliza tabla--> 
        <?php               
    }                
}else{
    echo 'HA OCURRIDO UN ERROR AL CARGAR LA INFORMACIÓN.';
    $date= new DateTime();
    $format= $date->format("Y-m-d H:i:s");
    $fp= fopen('logs/error.log','a+');        
    fwrite($fp,'['. $format . ']' . "\t" . 'Error al cargar URL: ' . $url . PHP_EOL);
    fclose($fp);
}


//COTIZACIÓN POR BANCOS.--------------------------------------------------
$url= 'https://www.dolarsi.com/api/dolarSiInfo.xml';
$xml= file_get_contents($url);

if ($xml){
    $data= simplexml_load_string($xml);    

    if(is_object($data)){        
        ?>
        <!--Inicio tabla -->
        <h3 class="text-center">Cotización Bancos</h3>
        <div class="container text-center">        
            <table class="table table-responsive table-bordered table-sm table-xs">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Compra</th>
                        <th class="text-center">Venta</th>
                    </tr>
                </thead>
                <?php
                //Mostrar cotizaciones de las siguientes casas.
                $casas= array('casa22','casa6','casa217','casa336','casa342','casa401');
                for($i=0; $i<count($casas); $i++){
                    ?>
                    <tbody>
                        <tr>
                            <td><?php echo $i+1 ?></td>
                            <td><?php echo $data->Capital_Federal->{$casas[$i]}->nombre;  ?></td>
                            <td><?php echo $data->Capital_Federal->{$casas[$i]}->compra;  ?></td>
                            <td><?php echo $data->Capital_Federal->{$casas[$i]}->venta;  ?></td>                                                
                        </tr>
                    </tbody>
                    <?php
                }
                ?>                                                        
            </table>        
        </div>
        <!--Fin tabla-->
        <!--RIESGO PAÍS.-------------------------------------------------- -->
        <!--Inicio tabla -->
        <h3 class="text-center">Riesgo País</h3>
        <div class="container text-center">        
            <table class="table table-responsive table-bordered table-sm table-xs">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Puntos</th>                        
                    </tr>
                </thead>
                <?php
                //Mostrar riesgo país de las siguientes casas.                
                $casas= array('casa141','casa142','casa143','casa144');
                for($i=0; $i<count($casas); $i++){
                    ?>
                    <tbody>
                        <tr>
                            <td><?php echo $i+1 ?></td>
                            <td><?php echo $data->Riesgo_pais->{$casas[$i]}->nombre;  ?></td>
                            <td><?php echo $data->Riesgo_pais->{$casas[$i]}->compra;  ?></td>                            
                        </tr>
                    </tbody>
                    <?php
                }
                ?>                                                        
            </table>        
        </div>
        <!--Fin tabla-->  
        <?php                                                                    
    }                
}else{
    echo 'HA OCURRIDO UN ERROR AL CARGAR LA INFORMACIÓN.';
    $date= new DateTime();
    $format= $date->format("Y-m-d H:i:s");
    $fp= fopen('logs/error.log','a+');    
    fwrite($fp,'['. $format . ']' . "\t" . 'Error al cargar URL: ' . $url . PHP_EOL);
    fclose($fp);
}
?>
